FROM registry.gitlab.com/cmunroe/tf2-thin

USER root
WORKDIR /opt/tf/addons/sourcemod/scripting

RUN rm -rf /opt/tf/addons/sourcemod/plugins/* && \
apt update && \
apt install libc6:i386 libstdc++6:i386 -y

CMD ./compile.sh